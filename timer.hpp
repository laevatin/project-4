#pragma once
#include <chrono>
#include <list>
#include <memory>

using namespace std;
class Timer {
public: 

    Timer( bool start = false );
    virtual ~Timer();

    void start(); 
    void stop( bool restart = false ); 
    void stop( size_t iterations,bool restart = false ); 
    void reset();

    double totalTime(); 
    double averageTime();

    list<double>::iterator begin(); 
    list<double>::iterator end();

    int getIterations();

    void sort();

private:
    list<double> timelist;
    chrono::high_resolution_clock::time_point start_time;

    bool status;
    double totaltime;
    int iteration;
};