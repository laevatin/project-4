#include <stdlib.h>
#include <iostream>
#include <chrono>

using namespace std;
using namespace chrono;
// Helper functions for task 1.
// Long operation to time
int dummyFunction(int n);

int fib(int n) {
    return dummyFunction(n);
}

int dummyFunction(int n) {
    if (n < 2) {
      return n;
    } else {
      return fib(n-1) + fib(n-2);
    }
}


void task1() {
    /* ****************** TASK 1  std::chrono ********************** *
     * Follow the instructions to test how much time it could take   *
     * by running dummyFunction(40);                                 *
     *                                                               *
     * Filling the places with ...                                   *
     * ************************************************************* */

    // Set the start time
    auto start_time = high_resolution_clock::now();

    dummyFunction(40);

    // Set the end time
    auto end_time = high_resolution_clock::now();

    // Set the duration
    auto time = end_time - start_time;

    // Output the time in microsecond
    std::cout << "fib(40) took " <<
    duration_cast<microseconds>(time).count() << " microsecond to run.\n";
}
//task1 done
int main(int argc, char *argv[])
{
    task1();

    return 0;
}