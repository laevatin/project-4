#include "Timer.hpp"

using namespace chrono;

Timer::Timer( bool start ){
    iteration = 0;
    totaltime = 0;

    if (start == true){
        status = true;
        start_time = high_resolution_clock::now();
    }
}

void Timer::start(){
    if (status == true){
        return;
    }
    status = true;
    start_time = high_resolution_clock::now();
}

void Timer::stop( bool restart ){
    if (status == false){
        return;
    }
    auto deltatime = high_resolution_clock::now() - start_time;
    double time_second = (double)duration_cast<nanoseconds>(deltatime).count() / 1000000000.0;
    timelist.push_back(time_second);
    totaltime += time_second;
    iteration++;
    status = false;

    if (restart == true){
        start();
    }
}

void Timer::stop( size_t iterations,bool restart ){
    if (status == false){
        return;
    }
    auto deltatime = high_resolution_clock::now() - start_time;
    double time_second = (double)duration_cast<nanoseconds>(deltatime).count() / 1000000000.0;
    timelist.push_back(time_second);
    totaltime += time_second;
    iteration = iterations;
    status = false;
    if (restart == true){
        start();
    }
}

void Timer::reset(){
    timelist.clear();
    status = false;
    iteration = 0;
    totaltime = 0;
}

double Timer::totalTime(){
    return totaltime;
}

double Timer::averageTime(){
    if (iteration == 0){
        return 0;
    } 
    return totaltime / (double)iteration;
}

list<double>::iterator Timer::begin(){
    return timelist.begin();
}

list<double>::iterator Timer::end(){
    return timelist.end();
}

int Timer::getIterations(){
    return iteration;
}

void Timer::sort(){
    timelist.sort();
}

Timer::~Timer(){
    
}
