#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include "Timer.hpp"
#include <iomanip>
#include <chrono>
using namespace std;
using namespace chrono;

// Helper class for task 4.
class LargeObject {
public:
    LargeObject();
    virtual ~LargeObject();

private:
    int m_data[500000];
};

LargeObject::LargeObject() {};
LargeObject::~LargeObject() {};

void task4() {
    /* ************* TASK 4  Filling lists and vectors ************* *
     * Find the mean and median of the data in Task 3!               *
     * Copy your code from task 3 and finish the computation of      *
     * mean and median.                                              *
     *                                                               *
     * What can you observe? Will you change your conclusion?        *
     * ************************************************************* */

    LargeObject obj;

    //create a list and a timer for the list
    std::list<LargeObject> myList;
    /*
    auto start_time = high_resolution_clock::now();

    myList.push_back(obj);

    // Set the end time
    auto end_time = high_resolution_clock::now();

    // Set the duration
    auto time = end_time - start_time;
    std::cout << "xxx " <<
    duration_cast<nanoseconds>(time).count() << " microsecond to run.\n";
    */
    Timer listTimer;
    for( int i = 0; i < 100; i++ ) {
        listTimer.start();
        myList.push_back(obj);
        listTimer.stop();
    }

    //create a vector and a timer for the vector
    
    std::vector<LargeObject> myVector;
    Timer vectorTimer;
    for( int i = 0; i < 100; i++ ) {
        vectorTimer.start();
        myVector.push_back(obj);
        vectorTimer.stop();
    }

    listTimer.sort();
    vectorTimer.sort();

    //print all times for the list
    std::cout << "The times for the list are:\n";
    std::list<double>::iterator it1 = listTimer.begin();
    while( it1 != listTimer.end() ) {
        std::cout << fixed << setprecision(10) << *it1 << " "; it1++;
    }
    std::cout << "\n\n";

    //print all times for the vector
    
    std::cout << "The times for the vector are:\n";
    std::list<double>::iterator it2 = vectorTimer.begin();
    while( it2 != vectorTimer.end() ) {
        std::cout << fixed << setprecision(10) << *it2 << " "; it2++;
    }
    std::cout << vectorTimer.totalTime();
    std::cout << "\n\n";

    //print the means
    std::cout << "putting an element into a list takes in average " << fixed << setprecision(6) << listTimer.averageTime() << " seconds.\n";
    std::cout << "putting an element into a vector takes in average " << fixed << setprecision(6) << vectorTimer.averageTime() << " seconds.\n";

    //the median of the times is
    int mid = 0;
    std::list<double>::iterator it3;
    for ( it3 = listTimer.begin(); it3 != listTimer.end(); ++it3){
        mid++;
        if (mid == 24) break; 
    }
    std::cout << "the median time for putting an element into a list is " << fixed << setprecision(6) << *it3 << " seconds.\n";
    mid = 0;
    std::list<double>::iterator it4;
    for ( it4 = vectorTimer.begin(); it4 != vectorTimer.end(); ++it4){
        mid++;
        if (mid == 24) break; 
    }
    std::cout << "the median time for putting an element into a vector is " << fixed << setprecision(6) << *it4 << " seconds.\n";
}

int main(int argc, char *argv[])
{
    task4();

    return 0;
}