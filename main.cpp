#include <iostream>
#include <random>
#include <time.h>
#include <unistd.h>
#include <string>
#include "platform.hpp"
#include "State.hpp"
#include "Agents.hpp"

void runRandomAgent();
void runMonteCarloAgent(int);
void runMonteCarloAgentWithTimer(double);

int main(int argc, char *argv[])
{
    int opt = 0;
    std::string agent_name = "MonteCarloAgentWithTimer";
    int seed = time(NULL);
    int trial_time = 100;
    double time_limit = 0.01;
    while ((opt = getopt(argc, argv, "A:s:t:l:")) != -1) {
        switch (opt)
        {
        case 'A':
            agent_name = optarg;
            break;
        
        case 's':
            seed = atoi(optarg);
            break;

        case 't':
            trial_time = atoi(optarg);
            break;

        case 'l':
            time_limit = atof(optarg);
            break;
        }
    }

    srand(seed);
    
    if (agent_name == "RandomAgent")
        runRandomAgent();
    else if (agent_name == "MonteCarloAgent")
        runMonteCarloAgent(trial_time);
    else if (agent_name == "MonteCarloAgentWithTimer")
        runMonteCarloAgentWithTimer(time_limit);
    else
        std::cout << "No such agent! Please try again." << std::endl;

#ifdef _WIN32
    system("pause");
#endif

    return 0;
}


void runRandomAgent()
{
    system("mode con cols=40 lines=15");

    Game game;
    RandomAgent agent;
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}


void runMonteCarloAgent(int trial_time)
{
    system("mode con cols=40 lines=15");

    Game game;
    MonteCarloAgent agent;
    agent.setTrialTime(trial_time);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}

void runMonteCarloAgentWithTimer(double time_limit)
{
    system("mode con cols=40 lines=15");

    Game game;
    MonteCarloAgentWithTimer agent;
    agent.setTimeLimit(time_limit);
    game.print();

    while (!game.isDead())
    {
        State state(game);
        char action = agent.getAction(state);
        if (!game.isPossibleArrow(action))
        {
            std::cout << "Warning: Illegal Action !" << std::endl;
            exit(1);
        }
        
        game.Move(action);
        game.RandomAdd();
        game.print();
    }
}